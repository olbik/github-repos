import { handleActions } from 'redux-actions';
import merge from 'lodash/merge';

import {
  setUserLogin,
  fetchUserDataRequest,
  fetchUserDataSuccess,
  fetchUserDataFailure,
  fetchUserReposSuccess,
  increaseUserReposPage,
} from '../actions/repos';

const defaultState = {
	userLogin: '',
	users: {},
};

export default handleActions({
	[setUserLogin]: (state, { payload: userLogin }) => merge({}, state, {
		userLogin,
	}),
	[fetchUserDataRequest]: (state, { payload: userLogin }) => merge({}, state, {
		userLogin,
		users: {
			[userLogin]: {
				loading: true,
				loaded: false,
				error: '',
				details: {},
        page: 0,
				pageLoading: true,
        repos: [],
			},
		},
	}),
	[fetchUserDataSuccess]: (state, { payload: { userLogin, details, repos }}) => merge({}, state, {
		users: {
			[userLogin]: {
				loading: false,
				loaded: true,
				details,
				pageLoading: false,
				repos,
			},
		},
	}),
	[fetchUserDataFailure]: (state, { payload: { userLogin, error } }) => merge({}, state, {
		users: {
			[userLogin]: {
				loading: false,
				error,
			},
		},
	}),
  [fetchUserReposSuccess]: (state, { payload: { userLogin, repos }}) => merge({}, state, {
		users: {
			[userLogin]: {
				pageLoading: false,
				repos: [...state.users[userLogin].repos, ...repos],
			},
		},
	}),
	[increaseUserReposPage]: (state, { payload: userLogin }) => merge({}, state, {
		users: {
			[userLogin]: {
				pageLoading: true,
        page: state.users[userLogin].page + 1,
			},
		},
	}),
}, defaultState);
