import axios from 'axios';
import config from '../config';

export const request = axios.create({
  timeout: 10000,
  headers: {
    'Authorization': 'token f263f6ffdc9fb653cde9b164c427fcdb3fd83ffe',
  },
})

const getErrorMessage = (error) => {
  if (error.response && error.response.data && error.response.data.message) {
    return error.response.data.message;
  }
  return error.message;
}

const getGitHubUsers = phrase => new Promise((resolve, reject) => {
  request
    .get(`${config.githubApiAddress}/search/users?q=${phrase}`)
    .then(response => resolve(
      response.data.items
        .filter((item, idx) => item.login.indexOf(phrase) !== -1)
        .filter((item, idx) => idx < 10)
    ))
    .catch(error => reject(getErrorMessage(error)));
});

const getGitHubUserDetails = userLogin => new Promise((resolve, reject) => {
  request
    .get(`${config.githubApiAddress}/users/${userLogin}`)
    .then(response => resolve(response.data))
    .catch(error => reject(getErrorMessage(error)));
});

const getGitHubUserRepos = (userLogin, page) => new Promise((resolve, reject) => {
  request
    .get(`${config.githubApiAddress}/users/${userLogin}/repos?page=${page + 1}&per_page=${config.reposPerPage}`)
    .then(response => resolve(response.data))
    .catch(error => reject(getErrorMessage(error)));
});

const api = {
  getGitHubUsers,
  getGitHubUserDetails,
  getGitHubUserRepos,
};

export default api;
