import React, { Component } from 'react'; //eslint-disable-line
import styles from './AutocompleteItem.css';

export default ({ login, avatar_url }) => (
  <div>
    <img className={styles.avatar} src={avatar_url} alt={login} />
    <div className={styles.login}>{login}</div>
  </div>
);
