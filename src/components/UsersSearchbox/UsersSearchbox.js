import React, { Component } from 'react'; //eslint-disable-line
import { Link, browserHistory } from 'react-router';
import debounce from 'lodash/debounce';
import AutoComplete from 'material-ui/AutoComplete';
import MenuItem from 'material-ui/MenuItem';
import api from '../../api';
import AutocompleteItem from './AutocompleteItem/AutocompleteItem';
import styles from './UsersSearchbox.css';

class UsersSearchbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phrase: '',
      dataSource: [],
    };
  }

  handleUpdateInput = async (phrase) => {
    if (phrase === '') {
      this.setState({ phrase, dataSource: [] });
      return;
    }
    const data = await api.getGitHubUsers(phrase);
    const dataSource = data.map(item => ({
      data: item,
      text: item.login,
      value: (<MenuItem primaryText={<AutocompleteItem {...item} />} />),
    }));
    this.setState({ phrase, dataSource });
  };

  handleItemSelected = (item) => {
    const user = typeof item === 'string' ? item : item.data.login;
    browserHistory.push(`/user/${user}`);
    this.setState({ phrase: '' });
  }

  render() {
    const { phrase, dataSource } = this.state;
    return (
      <div className={styles.wrap}>
        <div className={styles.content}>
          <Link to={'/'} className={styles.link}><h1 className={styles.logo}>GitHub Repos</h1></Link>
          <div className={styles.searchbox}>
            <AutoComplete
              id="UsersSearchboxAutocomplete"
              hintText="Search GitHub user"
              searchText={phrase}
              dataSource={this.state.dataSource}
              onUpdateInput={debounce(this.handleUpdateInput, 500)}
              onNewRequest={this.handleItemSelected}
              filter={AutoComplete.noFilter}
              fullWidth
            />
          </div>
        </div>
      </div>
    );
  }
}

export default UsersSearchbox;
