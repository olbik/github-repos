import React, { Component } from 'react'; //eslint-disable-line
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import InfiniteScroll from 'react-infinite-scroller';
import { fetchUserRepos } from '../../actions/repos';
import RepoDetails from './RepoDetails/RepoDetails'
import styles from './UserRepos.css';

class UserRepos extends Component {
  loadMore = () => {
    const { userLogin, fetchUserRepos, pageLoading } = this.props;
    if (pageLoading) return;
    fetchUserRepos(userLogin);
  }

  render() {
    const { repos, page, publicRepos } = this.props;
    return (
      <div>
        <div className={styles.title}>
          Repositories
          <span className={styles.amount}>{publicRepos}</span>
        </div>
        <InfiniteScroll
          initialLoad={false}
          pageStart={page}
          hasMore={repos.length < publicRepos}
          loadMore={this.loadMore}
          loader={<div className={styles.loader}>Loading...</div>}
        >
          {repos.map((item, idx) => <RepoDetails key={idx} {...item} />)}
        </InfiniteScroll>
      </div>
    );
  }
}

const selector = createSelector(
  state => state.repos.userLogin,
  state => state.repos.users[state.repos.userLogin] ? state.repos.users[state.repos.userLogin].page : 0,
  state => state.repos.users[state.repos.userLogin] ? state.repos.users[state.repos.userLogin].pageLoading : true,
  (userLogin, page, pageLoading) => ({ userLogin, page, pageLoading })
);

export default connect(selector, { fetchUserRepos })(UserRepos);
