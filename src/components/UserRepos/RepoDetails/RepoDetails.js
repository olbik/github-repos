import React, { Component } from 'react'; //eslint-disable-line
import relativeDate from 'relative-date';
import styles from './RepoDetails.css';

export default ({ name, description, updated_at, language, html_url }) => (
  <div className={styles.wrap}>
    <a className={styles.name} href={html_url} target="_blank" dangerouslySetInnerHTML={{ __html: name }} />
    {description && <div className={styles.desc} dangerouslySetInnerHTML={{ __html: description }} />}
    <div className={styles.info}>
      <span className={styles.updated}>Updated {relativeDate(new Date(updated_at))}</span>
      {language && <span dangerouslySetInnerHTML={{ __html: language }} />}
    </div>
  </div>
);
