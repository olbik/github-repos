import React, { Component } from 'react'; //eslint-disable-line
import relativeDate from 'relative-date';
import cx from 'classnames';
import styles from './UserDetails.css';

export default ({ avatar_url, blog, email, location, login, name, html_url, created_at }) => (
  <div>
    <div className={styles.asideLeft}>
      <a href={html_url} target="_blank" className={styles.link}>
        <img className={styles.avatar} src={avatar_url} />
      </a>
    </div>
    <div className={styles.asideRight}>
      {name && (
        <a href={html_url} target="_blank" className={styles.link}>
          <div className={styles.name} dangerouslySetInnerHTML={{ __html: name }} />
        </a>
      )}
      <a href={html_url} target="_blank" className={styles.link}>
        <div className={styles.login} dangerouslySetInnerHTML={{ __html: login }} />
      </a>
      {location && <div className={styles.info} dangerouslySetInnerHTML={{ __html: location }} />}
      {email && <a href={`mailto:${email}`} target="_blank" className={cx(styles.info, styles.oneline)} title={email} dangerouslySetInnerHTML={{ __html: email }} />}
      {blog && <a href={blog} target="_blank" className={cx(styles.info, styles.oneline)} title={blog}>{blog}</a>}
      <div className={styles.info}>Joined {relativeDate(new Date(created_at))}</div>
    </div>
  </div>
);
