import React, { Component } from 'react'; //eslint-disable-line
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import UsersSearchbox from '../UsersSearchbox/UsersSearchbox';
import styles from './Wrapper.css';

class Wrapper extends Component {
  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme({ userAgent: 'all' })}>
        <div>
          <UsersSearchbox />
          <div className={styles.content}>
            {this.props.children}
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default Wrapper;
