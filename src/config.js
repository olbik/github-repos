export const config = {
  githubApiAddress: 'https://api.github.com',
  reposPerPage: 20,
};

const getConfig = () => {
  if (process.env.__APP__CLIENT__) {
    return window.appConfig.config;
  }
  return config;
};

export default getConfig();
