import './styles.css';
import { runClient } from './app/runClient';
import injectTapEventPlugin from 'react-tap-event-plugin';
process.env.NODE_PATH = __dirname;
injectTapEventPlugin(); // http://stackoverflow.com/a/34015469/988941
runClient();
