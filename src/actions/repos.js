import { createAction } from 'redux-actions';
import api from '../api';

export const setUserLogin = createAction('repos/setUserLogin');
export const fetchUserDataRequest = createAction('repos/fetchUserDataRequest');
export const fetchUserDataSuccess = createAction('repos/fetchUserDataSuccess');
export const fetchUserDataFailure = createAction('repos/fetchUserDataFailure');

export const fetchUserData = userLogin => async (dispatch, getState) => {
  dispatch(setUserLogin(userLogin));
  if (getState().repos.users[userLogin]) return;
  dispatch(fetchUserDataRequest(userLogin));
  try {
    const details = await api.getGitHubUserDetails(userLogin);
    const repos = await api.getGitHubUserRepos(userLogin, 0);
    dispatch(fetchUserDataSuccess({ userLogin, details, repos }));
  } catch (error) {
    dispatch(fetchUserDataFailure({ userLogin, error }));
  }
};

export const fetchUserReposSuccess = createAction('repos/fetchUserReposSuccess');
export const increaseUserReposPage = createAction('repos/increaseUserReposPage');

export const fetchUserRepos = userLogin => async (dispatch, getState) => {
  dispatch(setUserLogin(userLogin));
  dispatch(increaseUserReposPage(userLogin));
  try {
    const repos = await api.getGitHubUserRepos(userLogin, getState().repos.users[userLogin].page);
    dispatch(fetchUserReposSuccess({ userLogin, repos }));
  } catch (error) {
    console.error(error); // FIXME
  }
};
