import React from 'react'; //eslint-disable-line
import Helmet from 'react-helmet';

export default () => (
  <div>
    <Helmet title="GitHub Repos" />
  </div>
);
