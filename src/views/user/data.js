import {
  fetchUserData,
} from '../../actions/repos';

const action = ({ dispatch, params }) => Promise.all([
  dispatch(fetchUserData(params.userLogin)),
]);

export const hooks = {
  fetch: action,
  defer: action,
};
