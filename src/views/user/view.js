import React, { Component } from 'react'; //eslint-disable-line
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Link } from 'react-router';
import Helmet from 'react-helmet';
import UserDetails from '../../components/UserDetails/UserDetails';
import UserRepos from '../../components/UserRepos/UserRepos';
import styles from './view.css';

class View extends Component {
  render() {
    const { userLogin, loading, loaded, error, details, repos } = this.props;
    const metaTitle = `${userLogin} - GitHub Repos`;
    if (loading) {
      return (
        <div className={styles.wrap}>
          <Helmet title={metaTitle} />
          <div className={styles.loader} />
        </div>
      );
    }
    if (error !== '') {
      return (
        <div className={styles.wrap}>
          <Helmet title={`${userLogin} - GitHub Repos`} />
          <div className={styles.errorTitle}>Error</div>
          <div className={styles.errorMsg}>
            {error === 'Not Found' && <span>GitHub user <strong>{userLogin}</strong> not found</span>}
            {error !== 'Not Found' && error}
          </div>
        </div>
      );
    }
    return (
      <div className={styles.wrap}>
        <Helmet title={metaTitle} />
        <div className={styles.userDetails}>
          <UserDetails {...details} />
        </div>
        <div className={styles.repos}>
          <UserRepos repos={repos} publicRepos={details.public_repos} />
        </div>
      </div>
    );
  }
}

const selector = createSelector(
  state => state.repos.userLogin,
  state => !state.repos.users[state.repos.userLogin]
    ? { loading: true, loaded: false, error: '', details: {}, repos: [] }
    : state.repos.users[state.repos.userLogin],
  (userLogin, userData) => ({ userLogin, ...userData })
);

export default connect(selector)(View);
