import React from 'react'; //eslint-disable-line
import { Route, IndexRoute } from 'react-router';
import Wrapper from '../components/Wrapper/Wrapper';
import Homepage from '../views/homepage';
import User from '../views/user';
import PageNotFound from '../components/PageNotFound/PageNotFound';

export default () => (
  <Route path="/" component={Wrapper}>
    <IndexRoute component={Homepage} />
    <Route path="/user/:userLogin" component={User} />
    <Route path="*" component={PageNotFound} />
  </Route>
);
